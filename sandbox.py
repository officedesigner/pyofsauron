###### Core objects and sets ######
from lib.core.object import * 
from lib.core.object_set import *
from lib.core.object_set_group import *
###### Core property and property set ######
from lib.core.props import *
from lib.core.prop import *
###### Core ActiveRecord ######
from lib.core.active_record import *
## INCLUDE ACTIVE REOCRD DESCENDANTS HERE
from lib.database.machine_data import *

#############################################
######   INITIALIZE CORE STUFF HERE    ######
ActiveRecord.load()
#############################################

###### LET'S DO THIS!!!!!!


#############################################
######   ActiveRecord Class def		   ######
#Normal declaration
class SomeTableName(ActiveRecord):
	pass
#Force no-plural table name
class SomeOtherTableNameWithNoPlural(ActiveRecord):
	pluralise = False
#############################################
######   ActiveRecord IN A NUTSHELL    ######

## Class configuration (MachineData as example

#Debug mode: Prints each query from an object if
# it is true. SET at ActiveRecord for all objects
# or at descendant level for filtered debugging
MachineData.debug_mode = False
#Add condition to .next()
MachineData.next_filter = "id % 2 = 0"	#Only grab records with even numbered ids
#Cache set of records for .next() rather than querying each record in isolation
MachineData.next_cache_size = 50 #Retrieve next() records in groups of 50
#Set the default properties to be returned from every query, default value is " * " 
MachineData.default_selects= " * "
#############################################
######   	Querying ActiveRecord	   ######
##	NOTE: Currently support [tuple] AND 
## 	or wild (atomic) str querying
##	NOTE: It's up to you to work with the 
##	table values constraints, NOT NULL etc
#Retrieve next object from the database
mData = MachineData.next()
#Find by property
mData = MachineData.find_by([["id", 1000]])
mData = MachineData.find_by("id = 1000")
#Find by property, return standard dictionary
mData = MachineData.find_dict_by("id = 1000")
#Find by multiple properties 
mData = MachineData.find_by([["id", 1000], ["ber", 60]])
mData = MachineData.find_by("id = 1000 AND ber = 60")
#Find set return list of ActiveRecords
mData = MachineData.find_all_by([["id", 1000]])
#Find set return dictionary list
mData = MachineData.find_all_dict_by("id = 1000")
#Create new record with synchronous association with the database
mData = MachineData.create([["ber", 60], ["ser", 50]])
#Create new with asynchronous association with the database. Call .save() to cement
mData = MachineData([["ber", 60], ["ser", 50]])
#Save current record state
mData.save()
#Get property value
val = mData["ber"]
val = mData.ber
#Set property value. Don't forget to .save() when the update's ready
mData["ber"] 	= 60
mData.ber		= 60
####
