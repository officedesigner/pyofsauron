from .props import *
from .object_set import *
import re 
class Object():
	#Got to start some with, __init__...
	def __init__(self, model, name="default", props=dict()):
		#Object alias, ideall unique
		self.name = name
		#Property set bound to the object
		self.props = Props(props)
		#Associated model
		#	Permits model referencing and various (get from sets I'm in etc... access)
		self.model = model
		#Add self to the associated model, I'm not there already
		self.model.addObject(self)
	#Convert object to string
	def __str__(self):
		str(self.props)
	#Retrieve property from object
	#	GLobal
	#	Instance
	#	Props
	#	Methods
	#
	#	Dotted access included
	#		myObject.propertyKey
	def __getattr__(self,name):
		#Global values and methods
		if name in vars(self.__class__):
			return getattr(self,name)
		#Instance dictionary, not exclusive to Props set
		if name in self.__dict__:
			return self.__dict__[name]
		#Property from actual property set
		if name in self.props:
			return self.props[name]
		#The entire property set
		if name == "props":
			return self.props
		#Property not found
		return None
	#Subcripted property access
	#	Square bracket access to properties
	#		myObject[propertyKey]
	def __getitem__(self,name):
		return self.__getattr__(name)
	#Property set contains key
	#	if key in myObject:
	#	for key in myObject:
	def __contains__(self, name):
		return name in self.props is not None
	#Set property in property set
	#	Object properties only! 
	#		Akin to  myObject.props[key] = value
	#		Or myObject.props.key = value
	def __setattr__(self, name, value):
		#Go Nuclear! Replace the entire prop set
		if name is "props":
			self.__dict__["props"] = value
			return
		#Ratify.... Change single property of the instance
		if name in getattr(self, "props"):
			self.__dict__["props"][name] = value
	
		