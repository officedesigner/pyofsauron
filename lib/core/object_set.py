class ObjectSet():
	#Create empty set
	def __init__(self):
		self.objects = []
	#Subscripted access
	def __getitem__(self,id):
		return self.objects[id]
	#Set length
	def __len__(self):
		return len(self.objects)
	#Iteration: DON'T USE ME!!! something you love will leave you if you do!
	def __iter__(self):
		return self.objects.__iter__()
	#Object is in the set
	def __contains__(self, test):
		for obj in self.objects:
			if obj.name == test.name:
				return True
	#####################################################
	###### General comment on proceeding functions ######
	## If the method has "name", "wieght",... as parameters, 
	##	treat as if it's a weighted formula
	##	For example: self.sum("area", "light_lux") on a set
	##		of two objects with those properties would be
	##		SUM[o.area * o.light_lux] 
	##		or SUM[o["area"] * o["light_lux"]]
	#####################################################

	#Get the sum of the named property of all objects in the set
	#	name 	= primaryPropertyOrMethodName
	#	weight	= secondaryPropertyOrMethodName
	#
	#	This abuses the subscripted and dotted access feature built into
	#		the rest of the core library, Props, Object, ActiveRecord
	#		to accommodate dynamic accessor-based aggregation or whatever
	def sum(self,name,weight=False):
		sum = decimal.Decimal(0)
		for obj in self.objects:
			if weight.__class__ is not str:	
				w = weight if  weight is not False else decimal.Decimal(1)
			else:
				w = obj[weight] if obj[weight] is not None else 0
			temp = obj[name]
			sum += (temp if temp is not None else decimal.Decimal(0)) * w
		return sum
	#Area-weighted average from primary and weighting key
	def weighted_average(self,name, weightKey="area"):
		value = 0
		weight = 0
		for obj in self.objects:
			value += (obj[name] if obj[name] is not None else decimal.Decimal(0)) * obj[weightKey]
			weight += obj[weightKey]
		return value / weight
	#It's not fair.... Mean value
	def mean(self,name):
		return self.sum(name) / len(self.objects)
	#Median value.
	def median(self,name):
		values = []
		for obj in self.objects:
			values.append(obj[name] if obj[name] is not None else decimal.Decimal(0))
		values  = sorted(values)
		return values[int(len(values) / 2 - 1)] + values[int(len(values) / 2)] / 2 if len(values) % 2 == 0 else values[int((len(values) - 1) / 2 )]

	#Dot product akin to SUM (obviously) but for multiplying recklessly
	def product(self,name, weight = False):
		output = decimal.Decimal(1)
		for obj in self.objects:
			if weight.__class__ is not str:	
				w = weight if  weight is not False else decimal.Decimal(1)
			else:
				w = obj[weight] if obj[weight] is not None else 0
			temp = obj[name]
			output *= (temp if temp is not None else decimal.Decimal(0)) * w
		return output
		
	#Sum two separate sums (as per self.sum) and return the Quotient
	def divideSums(self,name1, name2, weight1=False, weight2=False):
		sum2 = self.sum(name2, weight2)
		return  self.sum(name1, weight1) / sum2 if sum2 > 0 else decimal.Decimal(0)
		
	########## Grouping and filtering ############
	##	Features for producing filtered sets or ##
	##	or collections of sets (ObjectSetGroup) ##
	##											##
	## 	this is also where the chained magic	##
	## 	happens.								##
	##############################################
	#Using the named property, create an ObjectSetGroup.
	# For example, say the objects in this set have the property "flavour"
	#	and there are three flavours "banana", "milk", "chocolate"
	#	the returned ObjectSetGroup will have three keys, one for each flavour
	#	where each group object key is bound to a 0 or longer __len__ ObjectSet
	#		self.groupBy("flavour")["chocolate"].groupBy("isKitKat")[True]. and so forth
	def groupBy(self,name):
		outputs = ObjectSetGroup(name)
		for obj in self.objects:
			val = obj[name] if obj[name] is not None else None
			if val in outputs:
				outputs[val].append(obj)
			else:
				outputs[val] = self.__class__()
				outputs[val].append(obj)
		return outputs
	#Filter objects by their class and only return those which are
	#	of the requested class.
	#		For example with SBEM you might say for a set of (SbemZone, SbemWall, SbemZone)
	#		self.filterByClass(SbemZone) and receive a new ObjectSet containing only
	#		the two SbemZone objects
	def filterByClass(self,cls):
		output = self()
		for obj in self.objects:
			if obj.__class__ is cls:
				output.append(obj)
		return output
	#Filter by class and property value
	#	As above but with a property value conditional
	#	For example using the same set as above (SbemZone, SbemWall, SbemZone)
	#		giving the each SbemZone values for 'sampleProperty' ov 1 and 2
	#		respectively.
	#			self.filterByClassAndProperty(SbemZone,"sampleProperty",1)#
	#			will return only the first SbemZone within the returned
	#			ObjectSet instance
	def filterByClassAndProperty(self, cls, prop, val):
		output = self()
		for obj in self.objects:
			if obj.__class__ is cls and obj[prop] == val:
				output.append(obj)
		return output
	#Add object to the set as you would with an array.
	#	NOTE: This will fry your computer if the objects you append
	#		don't use the lib.core AR or Object instances
	#		it is dependent on subscripted access
	def append(self,object):
		for obj in self.objects:
			if obj.name == object.name:
				return False  
		self.objects.append(object)
	#Find the first object with a given name in the set
	#	NOTE: It is assummed that everyone will assign
	#		unique names to their objects.
	def findObject(self, name):
		for obj in self.objects:
			if obj.name == name:
				return obj
	#AS filterByClass
	#	Chicken or egg, who knows.
	#	Left in just in case it is used somewhere else.
	#	possibly a better name that filterByClass
	#
	#	Consider making this a filter by class string name.
	def findObjects(self,cls):
		output = self()
		for obj in self.objects:
			if obj.__class__ is cls:
				output.append(obj)
		return output
	#Print properties to the console
	def print(self,name):
		for obj in self.objects:
			print(obj.name + ":\t" + str(obj.area) + "\t " + str(obj[name]))
		