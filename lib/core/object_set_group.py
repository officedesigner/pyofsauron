from .props import *
class ObjectSetGroup():
	#Subscripted access to nest ObjectSets
	def __getitem__(self, key):
		if key not in self:
			return SbemObjectSet()
		return self.sets[key]
	#Initialise Group with parent key (some identifier, useful some times)
	def __init__(self, key):
		#The name of the property used to create the group
		#	For example, flavour as the key (see ObjectSet filter comments
		#		self.sets keys will be different flavours, "chocolate", "milk", etc
		self.key = key
		#META: Subscripted acess to the subscripted access,...
		self.sets = dict()
	#Subscripted access to introduce new ObjectSets to the Group
	def __setitem__(self, key, sbemObjectSet):
		if key not in self.sets:
			self.sets[key] = sbemObjectSet
	#Expected set contains the passed key
	#	For example: if "chocolate" in self:
	def __contains__(self, key):
		return key in list(self.sets)
	#Number of sets within the Group
	def __len__(self):
		return len(self.sets)
	#Juggle objects from each set into a new Group
	#	where the new Group's key is the passed key
	#	parameter
	def reGroupBy(self,key):
		output = self.__class__(key);
		for k in self.sets.keys():
			for obj in self.sets[k].objects:
				if obj[key] not in output:
					output[obj[key]] = SbemObjectSet()
				output[obj[key]].append(obj)
		return output
	#Pass a separate ObjectSet and merge it with the
	#	existing (if already existing) ObjectSets
	#	or creates a new set based on each Objects[key] value
	def mergeSbemObjectSet(self, set):
		groupSet = set.groupBy(self.key)
		for key, sset in groupSet.sets.items():
			if not self[key]:
				self[key] = SbemObjectSet()
			for obj in sset.objects:
				self[key].append(obj)
		return self
	#Print some set information to the console
	def print(self):
		for k, v in self.sets.items():
			print(str(k) + "\t " + str(len(v)))
		