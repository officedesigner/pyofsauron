#Semi-intelligent dict()
#Behaves like dictionary but automatically converts strings to floats if necessary
import re
import decimal
from lib.core.prop import *
class Props():
	
	#Retrieve property from Props set
	def __getitem__(self,item):
		if item in self.properties:
			return self.properties[item].value
	
	#Set value, either new Prop or update Prop
	def __setitem__(self, item, value):
		if item not in self.properties:
			self.properties[item] = Prop(item, value)
		else:
			self.properties[item].setValue(value)
	#Return Prop value
	def __getattr__(self,name):
		if name in self.__dict__["properties"]:
			return self.__dict__["properties"][name].value
		return None
	def __contains__(self, name):
		return name in self.properties
	#Initialise
	def __init__(self, values=None):
		self.properties = dict()
		if values is not None:
			for k, v in  values.items():
				self.properties[k] = Prop(k, v)
	#Convert property set to string
	def __str__(self):
		output = ""
		for key,prop in self.properties.items():
			output += prop.key + ": " + str(prop.value) + " - " + str(prop.type) + "\n"
		return output
