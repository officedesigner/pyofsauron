#Simple, semi-intelligent property
#	Take string or whatever and attempts to deleagte it to some data type
#	has key, type and value
#	NOTE: if you access these directly without retreival from 
#		Props object you must use myProp.value
#TODO: Add support for arrays, dicts and other mutable types
import re
import decimal
class Prop():
	#Class level quote stripping.
	#	If there's quotes in a stsring and this is true, they will be removed but 
	#	will remain a string. These will be resotre when using str(myProperty)
	strip_quotes = True
	#Match float / int from string
	NUMERIC = re.compile("^-?\d+\.?\d*$")
	#Create the single property effectively, key and weak or strong value
	def __init__(self, key, value):
		#The property's name
		self.ky = key
		#Quotes for in the property value - set elsewhere, needs scoped here though
		self.hasQuotes = False
		#Property type: int, str, float, decimal etc
		self.typ = self.getValueType(value)
		#The actual value
		self.val = self.getValueFromType(value)
	#Convert this object to a string
	def __str__(self):
		if self.hasQuotes:
			return "\"" + str(self.value) + "\""
		return str(self.value)
	#Set the value, 
	def setValue(self, value):
		self.val = self.getValueFromType(value)
	#Sef-explanatory @prperty decorators
	@property
	def value(self):
		return self.val
	@property
	def key(self):
		return self.ky
	@property
	def type(self):
		return self.typ
	#Determine type from value
	#	Throw something in and it'll be assigned a type based on its structure
	#	generally this'll be as is or either string or decimal.
	#	unless float is explicitly sent in
	def getValueType(self, value):
		if type(value) is not str: 
			if type(value) is int or type(value) is float:
				return decimal.Decimal
			return type(value)
		if self.NUMERIC.search(value):
			return decimal.Decimal
		if value[0] == "\"":
			self.hasQuotes = True
		return str
	#Set value, automatically deal with string to float conversion
	def getValueFromType(self, value):
		if value.__class__ is float or value.__class__ is int:
			return decimal.Decimal(value)
		if value.__class__ is str and self.NUMERIC.search(value):
			return decimal.Decimal(value)
		if value.__class__ is str and self.__class__.strip_quotes and self.hasQuotes:
			return value[1:-1]
		return value
	