###### Machine Data ######
#
# Stores all feature from a translated model. 
# Appending should not filter features.
# weatherKey is automatically converted to weather_XXX categorical value
from lib.core.active_record import *
class MachineData(ActiveRecord):
    pluralise = False

